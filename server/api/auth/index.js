const authenticate = require('./authentication')
const permit = require('./authorization')

const roles = {
  ADMIN: 'admin',
  USER: 'user',
  ALL: 'all',
  UNK: 'unk'
}

module.exports = { authenticate, permit, roles }
