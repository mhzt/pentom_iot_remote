const { sendError } = require('../utils/utils')
module.exports = function permit (...permittedRoles) {
  return (request, response, next) => {
    const { user } = request
    if ((user &&
      (permittedRoles.includes(user.role) || permittedRoles.includes('all'))) ||
      permittedRoles.includes('unk')) {
      next()
    } else {
      sendError(response, 403)
    }
  }
}
