const express = require('express')
const router = express.Router()

const { Device } = require('../../db')
const Pentom = require('../utils/Pentom')
const { permit: allow, roles: { USER } } = require('../auth')

const {
  sendError,
  sendSuccess } = require('../utils/utils')

router.post('/send', allow(USER), function send (req, res) {
  const { remote, cmd, device } = req.body
  if (req.user.devices.includes(device)) {
    Device.findOne({ _id: device })
      .then(device => {
        if (!device) {
          return sendError(res, 404, 'device not found', 'ستگاه مورد نظر یافت نشد.')
        }

        Pentom.runTelepathy(device.key, 'Do_IR_Code', cmd)
          .then(r =>
            sendSuccess(res, {
              remote,
              cmd,
              device,
              result: r.data.callNodeFunc
            }))
          .catch(err => sendError(res, 500, err.message))
      })
  }
})

router.post('/learn', allow(USER), function send (req, res) {
  const { device, start } = req.body
  if (req.user.devices.includes(device)) {
    Device.findOne({ _id: device })
      .then(device => {
        if (!device) {
          return sendError(res, 404, 'device not found', 'ستگاه مورد نظر یافت نشد.')
        }

        Pentom.runTelepathy(device.key, 'IR_Learn_Status', start ? '1' : '0')
          .then(r =>
            sendSuccess(res, {
              device,
              result: r.data.callNodeFunc
            }))
          .catch(err => sendError(res, 500, err.message))
      })
  }
})

module.exports = router
