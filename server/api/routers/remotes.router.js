const express = require('express')
const router = express.Router()

const {
  permit: allow,
  roles: { ADMIN, USER }
} = require('../auth')

const { Remote: Ent } = require('../../db')

const {
  generateResponse,
  fetchAndResponseWith,
  fetchFromDb,
  sendSuccess
} = require('../utils/utils')

router.get('/search', function findRemote (req, res) {
  const { text = '', skip = 0, limit = 100 } = req.query
  if (!text.length) return sendSuccess(res, [])

  generateResponse(res,
    Ent.find({ $text: { $search: text }, createdByUser: false })
      .skip(skip)
      .limit(limit))
})

router.get(
  ['/', '/:_id'],
  allow(ADMIN),
  fetchAndResponseWith(Ent, {
    p: true,
    q: true
  })
)

router.post('/', allow(ADMIN), function newEnt (req, res) {
  const newEnt = new Ent({ ...req.body })
  generateResponse(res, newEnt.save(), true)
})

router.patch('/:_id', allow(ADMIN), function editEnt (req, res) {
  const { _id } = req.params
  generateResponse(res,
    fetchFromDb(Ent.findOne({ _id }), res).then((r) => {
      Object.entries(req.body || {})
        .forEach(([key, value]) => { r[key] = value })
      return r.save()
    }))
})

router.delete('/:_id', allow(ADMIN), function deleteEnt (req, res) {
  const { _id } = req.params
  generateResponse(res, Ent.findOneAndDelete({ _id }), true)
})

router.post('/add', allow(USER), function addRemote (req, res) {
  const { id } = req.body

  fetchFromDb(Ent.findOne({ _id: id, createdByUser: false }), res)
    .then((remote) => {
      if (!req.user.remotes.includes(id)) {
        req.user.remotes.push(id)
        req.user.save()
          .then(r => sendSuccess(res, remote, 200, 'Remote added'))
      } else {
        sendSuccess(res, remote, 200, 'Remote already added')
      }
    })
})

router.post('/remove', allow(USER), function removeRemote (req, res) {
  const { id } = req.body

  fetchFromDb(Ent.findOne({ _id: id }), res)
    .then((remote) => {
      if (req.user.remotes.includes(id)) {
        req.user.remotes.splice(req.user.remotes.indexOf(id), 1)
        req.user.save()
          .then(r => sendSuccess(res, remote, 200, 'Remote removed'))
        if (remote.creator === req.user.id) {
          remote.deleted = true
          remote.save()
        }
      } else {
        sendSuccess(res, remote, 200, 'Remote not added')
      }
    })
})

router.post('/new', allow(USER), function newRemote (req, res) {
  const { title, description, layout, commands } = req.body
  const newRemote = new Ent({
    title,
    description,
    layout,
    commands,
    createdByUser: true,
    creator: req.user.id
  })

  generateResponse(res, newRemote.save(), true)
})

router.post('/edit/:id', allow(USER), function editRemote (req, res) {
  const { id } = req.params
  const { title, description, layout, commands } = req.body

  fetchFromDb(Ent.findOne({ _id: id, creator: req.user.id }), res)
    .then(remote => {
      remote.title = title || remote.title
      remote.description = description || remote.description
      remote.layout = layout || remote.layout
      remote.commands = commands || remote.commands

      generateResponse(res, remote.save(), true)
    })
})

module.exports = router
