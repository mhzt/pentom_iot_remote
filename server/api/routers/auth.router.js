const express = require('express')
const router = express.Router()

const { User } = require('../../db')

const {
  sendError,
  sendSuccess } = require('../utils/utils')

router.post('/login',
  function login (req, res) {
    const { phone, password } = req.body
    User.findOne({ phone: { number: phone } })
      .then(r => {
        if (!r) return sendError(res, 404, 'user not found', 'نام کاربری یا رمز عبور اشتباه است.')
        else {
          r.checkPassword(password)
            .then(r => sendSuccess(res, r))
            .catch(_ => sendError(res, 404, 'user not found', 'نام کاربری یا رمز عبور اشتباه است.'))
        }
      })
      .catch(_ => {})
  })

module.exports = router
