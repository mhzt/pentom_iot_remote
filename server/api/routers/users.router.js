const express = require('express')
const router = express.Router()

const { permit: allow, roles: { ADMIN, PRINCIPLE, USER, TEACHER, ACCOUNTANT } } = require('../auth')

const { User } = require('../../db')

const {
  generateResponse,
  fetchAndResponseWith,
  fetchFromDb,
  sendError } = require('../utils/utils')

router.get(['/', '/:_id'],
  allow(ADMIN, PRINCIPLE, TEACHER, ACCOUNTANT),
  fetchAndResponseWith(User, { p: true, q: true }))

router.patch('/:_id',
  allow(ADMIN, PRINCIPLE),
  function editUser (req, res) {
    const { _id } = req.params
    generateResponse(res,
      fetchFromDb(User.findOne({ _id }), res)
        .then(r => {
          if (r.role === ADMIN) {
            return sendError(res, 'Cannot edit admin')
          }

          if (req.body.phone) {
            r.phone = { number: req.body.phone, validated: false }
          }

          if (req.body.password) {
            r.password = req.body.password
          }

          Object.entries(req.body.profile || {})
            .forEach(([key, value]) => { r.profile[key] = value })
          return r.save()
        }))
  })

router.post('/',
  allow(ADMIN, PRINCIPLE),
  function newUser (req, res) {
    const { role = USER, phone: number, grade, password, ...profile } = req.body
    const newInstance = new User({
      role,
      phone: { number },
      password,
      profile
    })

    generateResponse(res, newInstance.save(), true)
  }
)

module.exports = router
