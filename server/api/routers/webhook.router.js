const express = require('express')
const router = express.Router()

const { Device } = require('../../db')

router.post('/learn', function getPurchase (req, res) {
  const { cmd, nodekey } = req.body
  Device.findOne({ key: nodekey })
    .then(device => {
      if (device && device.learning) {
        global.sio.to(device.user).emit('learn', { cmd })
      }
    })
  res.json({ result: 'ok' })
})

router.post('/sensors', function getPurchase (req, res) {
  const { data, nodekey } = req.body
  Device.findOne({ key: nodekey })
    .then(device => {
      if (device && device.user) {
        const [tmp, hum] = data.split(',').map(Number)
        device.sensors.temp.push({ d: tmp })
        device.sensors.hum.push({ d: hum })
        device.save()
      }
    })
  res.json({ result: 'ok' })
})

module.exports = router
