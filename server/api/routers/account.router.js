const express = require('express')
const router = express.Router()

const { permit: allow, roles: { ALL } } = require('../auth')

const { User, Device } = require('../../db')

const {
  generateResponse,
  sendError,
  fetchFromDb } = require('../utils/utils')
const { fixPhone } = require('../utils/helpers')

router.get('/',
  allow(ALL),
  (req, res) => generateResponse(res,
    req.user.populate('remotes').populate('devices').execPopulate())
)

router.post('/',
  function newUser (req, res) {
    if (!req.body.phone || !req.body.role) {
      sendError(res, 400, 'Role and phone are required.', 'نقش و شماره تلفن الزامی هستند.')
    } else if (req.body.role === 'admin' && !req.user.role === 'admin') {
      return sendError(res, 403)
    } else {
      const number = fixPhone(req.body.phone)
      if (!number) {
        return sendError(res, 400, 'Invalid phone number', 'شماره همراه نامعتبر است.')
      }

      const newUser = new User({
        ...req.body,
        phone: { number, validated: false }
      })

      generateResponse(res, newUser.save(), true)
    }
  }
)

router.post('/addDevice',
  function newDevice (req, res) {
    const { key } = req.body
    fetchFromDb(Device.findOne({ key }), res)
      .then(device => {
        if (!device.user) {
          device.user = req.user.id
          req.user.devies.push(device.id)
          Promise.all([ req.user.save(), device.save() ])
            .then(() => generateResponse(res, device))
            .catch(err => sendError(res, 500, err.message))
        } else {
          sendError(res, 400, 'Device already exists', 'دستگاه قبلا ثبت شده است.')
        }
      })
  }
)

router.patch('/',
  allow(ALL),
  function editUser (req, res) {
    const { phone, password, profile = {} } = req.body
    if (phone) {
      const number = fixPhone(phone)
      if (number) {
        req.user.phone = { number, validated: false }
      }
    }

    if (password) {
      req.user.password = password
    }
    Object.entries(profile)
      .forEach(([key, value]) => { req.user.profile[key] = value })

    return generateResponse(res, req.user.save())
  })

module.exports = router
