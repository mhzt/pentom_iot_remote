const fs = require('fs')
const path = require('path')

const express = require('express')
const bodyParser = require('body-parser')

const authenticate = require('./auth/authentication')
const { sendError } = require('./utils/utils')
const morgan = require('morgan')

const app = express.Router()

app.use(morgan('dev'))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(authenticate)

// loading routers
fs.readdir(path.join(__dirname, 'routers'), (_, files) => {
  files
    .filter(file => file.endsWith('.router.js'))
    .forEach(file => {
      const section = file.split('.')[0]
      app.use('/' + section, require(path.join(__dirname, 'routers', file)))
    })
})

app.use(function (err, req, res, next) {
  if (err instanceof SyntaxError && err.status === 400) {
    sendError(res, 400, 'Bad json')
  }
})

module.exports = app
