const request = require('request')

function runTelepathy (nodekey, func, arg) {
  const options = {
    method: 'POST',
    url: 'https://pentom.io/graphql',
    headers: {
      'Content-Type': 'application/json',
      'token': process.env.PENTOM_TOKEN
    },
    body: {
      query: `{callNodeFunc(nodeKey:"${nodekey}",funcKey:"${func}",arg:"${arg}")}`
    },
    json: true
  }

  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (error) return reject(error)
      resolve(body)
    })
  })
}

module.exports = {
  runTelepathy
}
