const errorDict = {
  400: 'Bad request',
  401: 'Unauthorized',
  402: 'Payment required',
  403: 'Forbidden',
  404: 'Not found',
  503: 'Internal server error',
  520: 'Unknown error'
}

const errorDictFa = {
  400: 'درخواست نامعتبر است.',
  401: 'شما به این صفحه دسترسی ندارید.',
  402: 'پرداخت نیاز است.',
  403: 'دسترسی غیر مجاز است.',
  404: 'موردی یافت نشد.',
  503: 'خطایی رخ داده است.',
  520: 'خطایی رخ داده است.'
}

const entDictFa = {
  user: 'کاربر',
  remote: 'کنترل'
}

const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1)

const responseMessage = (
  res,
  { type = 'success', statusCode, message, messageFa, data }
) => {
  let responseJson = {}

  if (type === 'error') {
    responseJson = {
      status: 'error',
      statusCode: statusCode || 520,
      error: message || errorDict[statusCode] || 'Unknown error',
      errorFa: messageFa || errorDictFa[statusCode] || 'خطایی رخ داده است.'
    }
  } else {
    responseJson = {
      status: 'success',
      statusCode: statusCode || 200,
      data: data,
      message: message || 'success',
      messageFa: messageFa || 'با موفقیت انجام شد.'
    }
  }

  return res.status(responseJson.statusCode).json(responseJson)
}

function parseError (msg) {
  if (!msg) return false
  if (msg.endsWith('ERRX1')) {
    return 'شماره تلفن قبلا ثبت شده است.'
  } else if (msg.endsWith('ERRX2')) {
    return 'شماره تلفن الزامی است.'
  } else if (msg.endsWith('ERRX3')) {
    return 'شماره تلفن معتبر نیست، شماره باید ۱۱ رقم باشد و با ۰۹ شروع شود.'
  }
  return false
}

const sendSuccess = (res, data, statusCode = 200, message) =>
  responseMessage(res, { type: 'success', statusCode, message, data })

const sendError = (res, statusCode, message, messageFa) =>
  responseMessage(res, {
    type: 'error',
    statusCode,
    message: message ? message.split('~')[0] : false,
    messageFa: messageFa || parseError(message)
  })

const notfound = (res, ent = '') => {
  const faMsg = entDictFa[(ent).toLowerCase()]
    ? entDictFa[ent.toLowerCase()] + ' مورد نظر یافت نشد.'
    : 'موردی یافت نشد.'

  sendError(res, 404, ent.length ? capitalize(ent) + ' not found' : 'Not found', faMsg)
}

const generateResponse = (res, p, single = false, ent) => {
  return p
    .then((r) => {
      if (!r || (single && Array.isArray(r) && !r.length)) return notfound(res, ent)
      return sendSuccess(res, single && Array.isArray(r) ? r[0] : r)
    })
    .catch((e) => {
      sendError(res, 400, e.message)
    })
}

const responseWith = (p) => {
  return (_, res) => generateResponse(res, p)
}

const fetchDocument = (
  model,
  { p = false, q = false, r = false },
  then = (_) => {}
) => {
  return (req, res) => {
    const query = {
      ...(p ? req.parama : {}),
      ...(q ? req.query : {}),
      ...(r ? req.roleFilter : {})
    }
    return [res, model.find(query).then(then)]
  }
}

function populateAll (m, populates = []) {
  if (!populates.length) return m
  const [currentPopulate, ...rest] = populates
  return populateAll(m.populate(currentPopulate), rest)
}

const fetchAndResponseWith = (
  model,
  { p = false, q = false, r = false, populates = [] }
) => {
  return (req, res) => {
    const query = {
      ...(p ? req.params : {}),
      ...(q ? req.query : {}),
      ...(r ? req.roleFilter : {})
    }

    Object.entries(query).forEach(([key, value]) => {
      if (typeof value === 'undefined') delete query[key]
    })

    return generateResponse(
      res,
      populateAll(model.find(query), populates),
      query.hasOwnProperty('_id'),
      model.modelName
    )
  }
}

const fetchFromDb = (p, res) => {
  return new Promise((resolve, reject) => {
    p.then((r) => {
      if (!r) {
        notfound(res)
        reject(new Error('404'))
      }
      return resolve(r)
    }).catch((e) => {
      sendError(res)
      reject(new Error('404'))
    })
  })
}

module.exports = {
  responseMessage,
  sendSuccess,
  sendError,
  notfound,
  responseWith,
  generateResponse,
  fetchAndResponseWith,
  fetchFromDb,
  fetchDocument
}
