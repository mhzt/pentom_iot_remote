const request = require('request')

const sendSms = (phone, text) => {
  console.log('[SMS] [LOG] [TXT]', phone, text, process.env.SMSUSER, process.env.SMSPASS, process.env.SMSNUM)

  request.post(
    {
      url: 'http://ippanel.com/api/select',
      body: {
        op: 'send',
        uname: process.env.SMSUSER,
        pass: process.env.SMSPASS,
        message: text,
        from: process.env.SMSNUM,
        to: [ phone ]
      },
      json: true
    },
    function (error, response, body) {
      if (error || !response.statusCode === 200) {
        console.log('[SMS] [ERR]', `[${response.statusCode}]`, error.message)
      } else {
        console.log('[SMS] [OK]', `[${response.statusCode}]`, body, phone)
      }
    }
  )
}

const sendManySms = (phones, text) => {
  request.post(
    {
      url: 'http://ippanel.com/api/select',
      body: {
        op: 'send',
        uname: process.env.SMSUSER,
        pass: process.env.SMSPASS,
        message: text,
        from: process.env.SMSNUM,
        to: phones
      },
      json: true
    },
    function (error, response, body) {
      if (error && !response.statusCode === 200) {
        console.log('[SMS] [ERR]', `[${response.statusCode}]`, error)
      }
    }
  )
}

const sendSmsPattern = (phone, pattern, inputData) => {
  console.log('[SMS] [LOG] [PTR]', phone, pattern, inputData, process.env.SMSUSER, process.env.SMSPASS, process.env.SMSNUM)

  request.post(
    {
      url: 'http://ippanel.com/api/select',
      body: {
        op: 'pattern',
        user: process.env.SMSUSER,
        pass: process.env.SMSPASS,
        patternCode: pattern,
        inputData: [inputData],
        fromNum: process.env.SMSNUM,
        toNum: phone
      },
      json: true
    },
    function (error, response, body) {
      if (error || !response.statusCode === 200) {
        console.log('[SMS] [ERR]', `[${response.statusCode}]`, error.message)
      } else {
        console.log('[SMS] [OK]', `[${response.statusCode}]`, body, phone)
      }
    }
  )
}

module.exports = { sendSms, sendManySms, sendSmsPattern }
