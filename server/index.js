if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

console.log('=======================================')

const path = require('path')

const express = require('express')
const cors = require('cors')
const port = process.env.PORT || 3000

const app = express()

require('./db')

app.use(cors())

app.get('/_check', (_, res) => res.status(200).send('up'))

app.use('/api/v1/', require('./api'))

app.use(express.static(path.join(__dirname, 'public')))
app.get('*', (_, res) =>
  res.sendFile(path.join(__dirname, 'public/index.html'))
)
console.log('[PUBLIC]', path.join(__dirname, 'public/index.html'))

app.listen(port, (_) => {
  console.log('[API]', port)
})
