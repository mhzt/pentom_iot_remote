const { Schema } = require('mongoose')

const TimeSeriesSchema = new Schema({
  t: { type: Date, default: Date.now },
  d: { type: Number, default: 0 }
})

const DeviceSchema = new Schema({
  info: { type: Schema.Types.Mixed, default: {} },
  key: { type: String, required: true },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  deleted: { type: Boolean, default: false },
  sensors: {
    temp: { type: [TimeSeriesSchema], default: [] },
    hum: { type: [TimeSeriesSchema], default: [] }
  }
}, { timestamps: true })

DeviceSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
  }
})

module.exports = DeviceSchema
