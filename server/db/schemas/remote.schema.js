const { Schema } = require('mongoose')

const RemoteSchema = new Schema({
  title: { type: String, default: 'عنوان جدید' },
  description: { type: String, default: '' },
  info: {
    vendor: { type: String, default: '' },
    category: { type: String, default: '' },
    subcategory: { type: String, default: '' }
  },
  tags: { type: [String], default: [] },
  layout: { type: String, default: '[]' },
  commands: { type: [
    {
      key: { type: String, default: '' },
      cmd: { type: String, default: '' }
    }
  ],
  default: [] },
  createdByUser: { type: Boolean, default: false },
  creator: { type: Schema.Types.ObjectId, ref: 'User' },
  deleted: { type: Boolean, default: false }
}, { timestamps: true })

RemoteSchema.index({ 'title': 'text' })

RemoteSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
  }
})

module.exports = RemoteSchema
