const { Schema, ObjectId } = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const bcrypt = require('bcrypt')

const jwt = require('jsonwebtoken')

const Phone = new Schema(
  {
    number: {
      type: String,
      lowercase: true,
      required: [true, 'is required.~ERRX2'],
      match: [/\d{11}/, 'is not valid.~ERRX3'],
      index: true
    },
    validated: { type: Boolean, default: false }
  },
  { _id: false }
)

const UserSchema = new Schema(
  {
    phone: { type: Phone, required: true, index: true, unique: true },

    password: { type: String, required: true },

    role: {
      type: String,
      enum: ['user', 'admin'],
      required: true
    },

    profile: {
      firstName: String,
      lastName: String,
      email: String,
      gender: { type: String, enum: ['male', 'female'], default: 'male' }
    },
    devices: { type: [{ type: ObjectId, ref: 'Device' }], default: [] },
    remotes: { type: [{ type: ObjectId, ref: 'Remote' }], default: [] },
    active: { type: Boolean, default: true }
  },
  { timestamps: true }
)

UserSchema.plugin(uniqueValidator, { message: 'must be unique.~ERRX1' })

UserSchema.pre('save', function (next) {
  if (!this.isModified('password')) {
    return next()
  }
  this.password = bcrypt.hashSync(this.password, 10)
  next()
})

UserSchema.methods.comparePassword = function (plaintext, callback) {
  return callback(null, bcrypt.compareSync(plaintext, this.password))
}

UserSchema.methods.checkPassword = function (plaintext) {
  return new Promise((resolve, reject) => {
    if (bcrypt.compareSync(plaintext, this.password)) {
      resolve({
        token: jwt.sign(
          { id: this._id },
          process.env.SECRET || 'wubba lubba dub dub'
        ),
        role: this.role
      })
    }
    reject(new Error('incorrect password.'))
  })
}

UserSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.password
    delete ret.__v

    if (ret.phone && ret.phone.number && ret.profile) {
      ret.profile.phone = ret.phone.number
    }

    if (ret.role !== 'user') {
      delete ret.remotes
    }
  }
})

module.exports = UserSchema
