const fs = require('fs')
const path = require('path')

const mongoose = require('mongoose')

mongoose.Promise = global.Promise

mongoose.connect(process.env.MONGO || 'mongodb://localhost:27017/myapp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}, (err) => {
  if (!err) {
    console.log('[MDB]', '[OK]', 'Connected to MongoDB')
  } else {
    console.log('[MDB]', '[ERR]', err)
  }
  console.log('[MDB]', '[DB]', process.env.MONGO || 'mongodb://localhost:27017/myapp')
})

// load schemas and generate models
const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1)
fs.readdirSync(path.join(__dirname, 'schemas'))
  .filter(file => file.endsWith('.schema.js'))
  .forEach(file => {
    const section = capitalize(file.split('.')[0])
    module.exports[section] = mongoose.model(section, require(path.join(__dirname, 'schemas', file)))
  })
