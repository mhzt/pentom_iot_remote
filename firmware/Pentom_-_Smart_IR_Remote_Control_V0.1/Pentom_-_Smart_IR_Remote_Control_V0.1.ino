#include <Pentom.h>
#include <Arduino.h>
#include "PinDefinitionsAndMore.h"
#include <IRremote.hpp>
#include <DHT.h>


struct storedIRDataStruct {
    IRData receivedIRData;
    uint8_t rawCode[RAW_BUFFER_LENGTH];
    uint8_t rawCodeLength;
} sStoredIRData;


#define DHTTYPE DHT21
#define DHTPIN  D1
DHT dht(DHTPIN, DHTTYPE, 11);

long           previousMillis = 0;
unsigned long  currentMillis = 0;
long           interval = 3000;

int DELAY_BETWEEN_REPEAT = 50;
float temperature;
float humidity;
bool learnStatus = false;

int RECV_PIN = 15;
byte SEND_PIN = 5;
IRrecv irrecv(RECV_PIN);
IRsend irsend(SEND_PIN);

void storeCode(IRData *aIRReceivedData);
void sendCode(storedIRDataStruct *aIRDataToSend);


void setup() {
  //Serial.begin(115200);

  Pentom.setFunction("Do_IR_Code", Do_IR_Code, API_DEVICE, "Do IR Code Callback");
  Pentom.setFunction("IR_Learn_Status", IR_Learn_Status, API_DEVICE, "IR Learn Status Callback");

  IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK);
  IrSender.begin(IR_SEND_PIN, ENABLE_LED_FEEDBACK);

  Serial.print(F("Ready to receive IR signals of protocols: "));
  printActiveIRProtocols(&Serial);
  Serial.print(F("at pin "));
  Serial.println(IR_RECEIVE_PIN);
  
  dht.begin();
  //Wire.begin();
}


void loop() {
  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    String dht_Data = Get_Temp_Humidity();
    Serial.println(dht_Data);
    Pentom.send("Temp_Hum_Data", dht_Data.c_str());
  }

  if (learnStatus) {
    if (IrReceiver.available()) {
        storeCode(IrReceiver.read());
        IrReceiver.resume(); // resume receiver
        Pentom.send("IR_Learn_Code", String(&sStoredIRData).c_str());
    }
  }
}


int Do_IR_Code(String data) {
  sendCode((IRData *)data);
  return 1;
}


int IR_Learn_Status(String data) {
  if (data == "1"){
    IrReceiver.start();
    learnStatus = true;
  }
  else if (data == "0"){
    IrReceiver.stop();
    learnStatus = false;
  }
  return 1;
}


void storeCode(IRData *aIRReceivedData) {
  if (aIRReceivedData->flags & IRDATA_FLAGS_IS_REPEAT) {
      Serial.println(F("Ignore repeat"));
      return;
  }
  if (aIRReceivedData->flags & IRDATA_FLAGS_IS_AUTO_REPEAT) {
      Serial.println(F("Ignore autorepeat"));
      return;
  }
  if (aIRReceivedData->flags & IRDATA_FLAGS_PARITY_FAILED) {
      Serial.println(F("Ignore parity error"));
      return;
  }

  sStoredIRData.receivedIRData = *aIRReceivedData;

  if (sStoredIRData.receivedIRData.protocol == UNKNOWN) {
      Serial.print(F("Received unknown code and store "));
      Serial.print(IrReceiver.decodedIRData.rawDataPtr->rawlen - 1);
      Serial.println(F(" timing entries as raw "));
      IrReceiver.printIRResultRawFormatted(&Serial, true);
      sStoredIRData.rawCodeLength = IrReceiver.decodedIRData.rawDataPtr->rawlen - 1;


      IrReceiver.compensateAndStoreIRResultInArray(sStoredIRData.rawCode);
  } else {
      IrReceiver.printIRResultShort(&Serial);
      sStoredIRData.receivedIRData.flags = 0;
      Serial.println();
  }
}

void sendCode(storedIRDataStruct *aIRDataToSend) {
  if (aIRDataToSend->receivedIRData.protocol == UNKNOWN) {
      // Assume 38 KHz
      IrSender.sendRaw(aIRDataToSend->rawCode, aIRDataToSend->rawCodeLength, 38);

      Serial.print(F("Sent raw "));
      Serial.print(aIRDataToSend->rawCodeLength);
      Serial.println(F(" marks or spaces"));
  } else {


      IrSender.write(&aIRDataToSend->receivedIRData, NO_REPEATS);

      Serial.print(F("Sent: "));
      printIRResultShort(&Serial, &aIRDataToSend->receivedIRData);
  }
}


String Get_Temp_Humidity() {
  String THData;
  float dht_temp;
  float dht_hum;
  dht_temp = dht.readTemperature();
  dht_hum = dht.readHumidity();
  
  if (!isnan(dht_temp)) {
    THData += String(dht_temp); 
    temperature = dht_temp;
  }
  else THData += String(temperature);
  
  THData += ",";
  
  if (!isnan(dht_hum)) {
    THData += String(dht_hum); 
    humidity = dht_hum;
  }
  else THData += String(humidity);
  
  return THData;
}
