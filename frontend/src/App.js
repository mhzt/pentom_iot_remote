import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Player from "./Page/Player";
import Tv from "./Page/Tv";
import { E } from "./Components/E";

import { AiOutlineStar } from "react-icons/ai";
import { FiMonitor } from "react-icons/fi";
import { BiSpeaker } from "react-icons/bi";
function App() {
  const cateItems = [
    { key:'/',id: 1, value: "پرکاربردها", icon: <AiOutlineStar /> },
    { key:'/tv',id: 2, value: "تلویزیون", icon: <FiMonitor /> },
    { key:'/player',id: 3, value: "سیستم صوتی", icon: <BiSpeaker /> },
  ];
  return (
    <Router>
      <E.Holder
        fd="column"
        jc="flex-start"
        ai="center"
        style={{ height: "100%" }}
      >
        <E.Header />
        <E.Categories items={cateItems} />
        <Switch>
          <Route path="/tv">
            <Tv />
          </Route>
          <Route path="/player">
            <Player />
          </Route>
          <Route path="/">
            <Tv />
          </Route>
        </Switch>
      </E.Holder>
    </Router>
  );
}

export default App;
