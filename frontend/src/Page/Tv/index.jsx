

import TurnOffComponent from "../../Components/TurnOffComponent";
import Menu from "../../Components/Menu";
import Volume from '../../Components/Volume'
function App() {

  return (
  <>

      <TurnOffComponent />
      <Menu />
      <Volume/>
    </>
  );
}

export default App;
