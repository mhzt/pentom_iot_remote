import TurnOffComponent from "../../Components/TurnOffComponent";
import Menu from "../../Components/Menu";
import Volume from "../../Components/Volume";
import Player from "../../Components/Palyer";
function App() {
  return (
    <>
      <TurnOffComponent />
      <Volume />
      <Player />
    </>
  );
}

export default App;
