import React from "react";
import styled from "styled-components";
import {AiOutlineMenu} from 'react-icons/ai'
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "55px",
  width: "55px",
  background: "white",
  borderRadius: "50px",
  boxShadow: "var(--shadow)",
  "&> svg": {
    color: "var(--black)",
    fontSize: "22px",
    cursor: "pointer",
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <AiOutlineMenu />
    </Holder>
  );
}
