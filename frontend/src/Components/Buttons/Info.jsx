import React from "react";
import styled from "styled-components";
import { BiCast } from "react-icons/bi";
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "55px",
  width: "55px",
  background: "white",
  borderRadius: "50px",
  boxShadow: "var(--shadow)",
  "&> span": {
    color: "var(--black)",
    fontSize: "13px",
    cursor: "pointer",
    fontFamily: "arial",
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <span>info</span>{" "}
    </Holder>
  );
}
