import React from "react";
import styled from "styled-components";
import {
  MdKeyboardArrowRight,
  MdKeyboardArrowLeft,
  MdKeyboardArrowDown,
  MdKeyboardArrowUp,
} from "react-icons/md";
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "150px",
  width: "150px",
  background: "white",
  borderRadius: "100px",
  boxShadow: "var(--shadow)",
  flexDirection: "column",
  "&> div": {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "50px",
    "&> svg": {
      fontSize: "19px",
      color: "var(--black)",
      cursor: "pointer",
    },
    "&> span": {
      fontFamily: "arial",
      cursor: "pointer",
    },
  },
  "&> .center": {
    justifyContent: "space-around",
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <div className="top">
        <MdKeyboardArrowUp />
      </div>
      <div className="center">
        <MdKeyboardArrowRight />
        <span>OK</span>
        <MdKeyboardArrowLeft />
      </div>
      <div className="bottom">
        <MdKeyboardArrowDown />
      </div>
    </Holder>
  );
}
