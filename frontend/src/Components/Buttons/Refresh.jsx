


import React from "react";
import styled from "styled-components";
import {GrRefresh} from 'react-icons/gr'
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "45px",
  width: "45px",
  background: "var(--back)",
  borderRadius: "50px",
  boxShadow: "var(--shadow)",
  "&> svg": {
    color: "var(--black)",
    fontSize: "17px",
    cursor: "pointer",
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <GrRefresh />
    </Holder>
  );
}
