


import React from "react";
import styled from "styled-components";
import {AiOutlineMinus} from 'react-icons/ai'
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "55px",
  width: "55px",
  background: "var(--back)",
  borderRadius: "50px",
  boxShadow: "var(--shadow)",
  "&> svg": {
    color: "var(--black)",
    fontSize: "17px",
    cursor: "pointer",
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <AiOutlineMinus />
    </Holder>
  );
}
