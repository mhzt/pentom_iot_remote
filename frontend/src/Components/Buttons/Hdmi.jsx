import React from "react";
import styled from "styled-components";
const Holder = styled.div({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "50px",
  width: "50px",
  background: "var(--back)",
  borderRadius: "50px",
  boxShadow: "var(--shadow)",
  "&> span": {
    color: "var(--black)",
    fontSize: "13px",
    cursor: "pointer",
    fontFamily:"arial"
  },
});
export default function TurnOff() {
  return (
    <Holder>
      <span>HDMI</span>
    </Holder>
  );
}
