import React from "react";
import { E } from "../E";
import styled from "styled-components";
import TurnOff from "../Buttons/TurnOff";
import Cast from '../Buttons/Cast';
import Hdmi from '../Buttons/Hdmi'
const Holder = styled.div({
  width: "calc( 100% - 70px )",
  minHeight: "30px",
  background: "white",
  margin: "20px",
  borderRadius: "25px",
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
  padding:"10px ",
  boxShadow:"var(--shadow)"
});
export default function index() {
  return (
    <Holder fd="column" jc="flex-start" ai="flex-start">
      <Hdmi />
      <Cast />
      <TurnOff />
    </Holder>
  );
}
