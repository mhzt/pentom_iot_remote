import React from "react";
import { E } from "../E";
import styled from "styled-components";
import EPG from '../Buttons/EPG';
import Numpad from '../Buttons/Numpad'
import Menu from '../Buttons/Menu';
import Info from '../Buttons/Info';
import Channel from '../Buttons/Channel';
const Holder = styled.div({
  width: "calc( 100% - 70px )",
  minHeight: "30px",
  margin: "20px",
  borderRadius: "25px",
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
  padding: "10px ",
  boxShadow: "var(--shadow)",
  "&> .right , .left":{
      display:"flex",
      flexDirection:"column",
      justifyContent:"space-around",
      alignItems:"center",
      minHeight:"150px"
  }
});
export default function index() {
  return (
    <Holder>
      <div className="right">
        <Numpad />
        <Info />
      </div>
      <div className="center">
        <Channel />
      </div>
      <div className="left">
        <EPG />
        <Menu />
      </div>
    </Holder>
  );
}
