import React, { useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import styled from "styled-components";
import { E } from "../E";


function checkLogined() {
  let res = localStorage.getItem("token");
  global.token = res !== null && res;
  if (res) {
    let grade = localStorage.getItem("role");
    global.cate = grade;
  }
  return res !== null;
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
  global.role = global.role || localStorage.getItem("role");

  if (!global.role) {
    window.location.href = "/auth/login";
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        checkLogined() ? (
          <E.Holder
            className={"Holder"}
            height="100%"
            dr={"row"}
            ai="flex-start"
            jc="start"
          >

            <E.God>
              <Component {...props} />
            </E.God>
          </E.Holder>
        ) : (
          <Redirect
            to={{
              pathname: "/auth/login",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

export default ProtectedRoute;
