import styled from "styled-components";
import Toastify from "toastify-js";
import "toastify-js/src/toastify.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { motion } from "framer-motion";
import "./index.css";
import { FiArrowRight, FiSearch, FiMenu } from "react-icons/fi";
import { RiEditBoxLine } from "react-icons/ri";
import moment from "jalali-moment";
import { useState } from "react";
global.toast = (text, color = "#6088f8") => {
  Toastify({
    text: text,

    duration: 3000,
    className: "toast",
    offset: {
      x: 20, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
      y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
    },
    backgroundColor: color,
  }).showToast();
};

global.asyncLocalStorage = {
  setItem: function (key, value) {
    return Promise.resolve().then(function () {
      localStorage.setItem(key, value);
    });
  },
  getItem: function (key) {
    return Promise.resolve().then(function () {
      return localStorage.getItem(key);
    });
  },
};

global.toFa = (num) => {
  if (typeof num !== "undefined") {
    const persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    return [...num.toString()]
      .map((c) => persianNumbers[parseInt(c)] || c)
      .join("");
  }
};

global.toEn = (str) => {
  const persianNumbers = [
    /۰/g,
    /۱/g,
    /۲/g,
    /۳/g,
    /۴/g,
    /۵/g,
    /۶/g,
    /۷/g,
    /۸/g,
    /۹/g,
  ];
  const arabicNumbers = [
    /٠/g,
    /١/g,
    /٢/g,
    /٣/g,
    /٤/g,
    /٥/g,
    /٦/g,
    /٧/g,
    /٨/g,
    /٩/g,
  ];

  if (typeof str === "string") {
    for (let i = 0; i < 10; i++) {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};

global.parseSearch = (search) => {
  return search
    .slice(1)
    .split("&")
    .reduce((previous, current) => {
      previous[current.split("=")[0]] = current.split("=")[1];
      return previous;
    }, {});
};

global.toFaDate = (d = Date.now(), f = "jYYYY/jMM/jDD − HH:mm") =>
  moment(d).format(f);

const Holder = styled.section({
  display: "flex",
  flexDirection: (p) => p.fd || "row",
  justifyContent: (p) => p.jc || "center",
  alignItems: (p) => p.ai || "center",
  flex: 1,
  background: (p) => p.bg || "var(--back)",
  borderRadius: (p) => p.br || "0",
});

const God = styled.section({
  flex: 1,
  flexDirection: "column",
  boxSizing: "border-box",
  display: "flex",
  justifyContent: "center",
  margin: "0px",
  height: "100vh",
  width: "100vw",
  overflowY: "scroll",
  transition: "500ms",
  alignItems: "center",
  "@media (max-width:850px)": {
    overflowX: "hidden",
  },
});

const Dl = styled.div`
  .dot-elastic {
    margin-top: 20px;
    position: relative;
    width: 8px;
    height: 8px;
    border-radius: 5px;
    background-color: ${(p) => p.color};
    color: ${(p) => p.color};
    animation: dotElastic 1s infinite linear;
  }

  .dot-elastic::before,
  .dot-elastic::after {
    content: "";
    display: inline-block;
    position: absolute;
    top: 0;
  }

  .dot-elastic::before {
    left: -15px;
    width: 8px;
    height: 8px;
    border-radius: 5px;
    background-color: ${(p) => p.color};
    color: ${(p) => p.color};
    animation: dotElasticBefore 1s infinite linear;
  }

  .dot-elastic::after {
    left: 15px;
    width: 8px;
    height: 8px;
    border-radius: 5px;
    background-color: ${(p) => p.color};
    color: ${(p) => p.color};
    animation: dotElasticAfter 1s infinite linear;
  }

  @keyframes dotElasticBefore {
    0% {
      transform: scale(1, 1);
    }
    25% {
      transform: scale(1, 1.5);
    }
    50% {
      transform: scale(1, 0.67);
    }
    75% {
      transform: scale(1, 1);
    }
    100% {
      transform: scale(1, 1);
    }
  }

  @keyframes dotElastic {
    0% {
      transform: scale(1, 1);
    }
    25% {
      transform: scale(1, 1);
    }
    50% {
      transform: scale(1, 1.5);
    }
    75% {
      transform: scale(1, 1);
    }
    100% {
      transform: scale(1, 1);
    }
  }

  @keyframes dotElasticAfter {
    0% {
      transform: scale(1, 1);
    }
    25% {
      transform: scale(1, 1);
    }
    50% {
      transform: scale(1, 0.67);
    }
    75% {
      transform: scale(1, 1.5);
    }
    100% {
      transform: scale(1, 1);
    }
  }
`;
const DotLoading = ({ color = "white" }) => {
  return (
    <Dl color={color}>
      <div
        style={{ color: color, backgroundColor: color }}
        class="dot-elastic"
      ></div>
    </Dl>
  );
};

const BtnHolder = styled.div({
  display: "block",
  backgroundColor: "var(--pink)",
  padding: "10px 30px",
  borderRadius: "20px",
  minWidth: "200px",
  height: "25px",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  "&> a": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    "&> svg": {
      color: "white",
    },
    "&> span": {
      color: "white",
      fontWeight: "bold",
      fontSize: "15px",
    },
  },
});
const Btn = ({ title, children, bg, textColor, to, onClick }) => {
  return (
    <BtnHolder textColor bg>
      <Link onClick={onClick} to>
        {children}
        <span>{title}</span>
      </Link>
    </BtnHolder>
  );
};

const Head = styled.div({
  display: "flex",
  flexDirection: "row",
  justifyContent: "flex-start",
  alignItems: "center",
  width: "100%",
  height: "70px",
  background: "white",
  "&> .title": {
    flex: "1",
    marginRight: "20px",
    "&> span": {
      fontFamily: "text",
      fontSize: "20px",
      fontWeight: "bold",
    },
  },
  "&> .icons": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "10px",
    "&> svg": {
      fontSize: "22px",
      color: "var(--textColorDark)",
      cursor: "pointer",
    },
  },
});
const Header = (props) => {
  return (
    <Head>
      <div className="title">
        <span>{props.title || "پارلار کنترل"}</span>
      </div>
      <div className="icons">
        <RiEditBoxLine onClick={props.editOnClick} />
        <FiMenu onClick={props.menuOnClick} />
      </div>
    </Head>
  );
};

const Cate = styled.div({
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "flex-start",
  flexDirection: "row",
  width: "100%",
  overflowX: "scroll",
  background: "white",
  "&> .item": {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: "5px 0px ",
    margin: "10px",
    color: "var(--gray)",
    width: "fit-content",
    transition: "500ms",
    "&> span": {
      display: "block",
      width: "fit-content",
      fontSize: "13px",
    },
  },
  "&> a[data-active='true']": {
    color: "black",
    transition: "500ms",

    borderBottomColor: "var(--lightBlue)",
    borderBottomStyle: "solid",
    borderBottomWidth: "1px",
  },
});
const Categories = (props) => {
  const [id, setId] = useState(1);
  return (
    <Cate>
      {props.items.map((item) => {
        return (
          <Link
            to={item.key}
            onClick={() => {
              setId(item.id);
            }}
            data-active={id == item.id}
            className="item"
          >
            {item.icon}
            <span>{item.value}</span>
          </Link>
        );
      })}
    </Cate>
  );
};
export const E = {
  Holder,
  God,
  Header,
  DotLoading,
  Btn,
  Categories,
};
