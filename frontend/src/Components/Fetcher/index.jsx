global.host =
  (window.location.hostname.includes("localhost")
    ? "http://localhost:3000"
    : "https://api.my.safirmazandaran.com/") + "/api/v1";

const req = (path = "", body = false, method = "GET", query = {}) => {
  let url = new URL(global.host + path);

  // add querr to existing url search params

  if (query) {
    Object.keys(query).forEach((key) => {
      url.searchParams.append(key, query[key]);
    });
  }

  return new Promise((resolve, reject) => {
    fetch(url, {
      headers: {
        "Content-Type": "application/json",
        "x-api-token": `${global.token}`,
      },
      ...(body
        ? {
            method: method,
            body: JSON.stringify(typeof body === "boolean" ? {} : body),
          }
        : { method: method }),
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.error) {
          if (r.error === "Unauthorized") {
            global.bridge.setToken(false);
            localStorage.removeItem("token");
            window.location.replace("/");
            return reject(new Error(r.error));
          } else {
            if (r.error == "Forbidden") {
              window.location.href = "/403";
            }
            return reject(new Error(r.errorFa || r.error));
          }
        }
        return resolve(r.data);
      })
      .catch((r) => reject(r));
  });
};

export const fetcher = req;
