import React from "react";
import { E } from "../E";
import styled from "styled-components";
import EPG from "../Buttons/EPG";
import Numpad from "../Buttons/Numpad";
import Menu from "../Buttons/Menu";
import Info from "../Buttons/Info";
import Channel from "../Buttons/Channel";

import Plus from "../Buttons/Plus";
import Minus from "../Buttons/Minus";
import Up from "../Buttons/Up";
import Down from "../Buttons/Down";
import Mute from "../Buttons/Mute";
import Refresh from "../Buttons/Refresh";
const Holder = styled.div({
  width: "calc( 100% - 70px )",
  minHeight: "30px",
  margin: "20px",
  borderRadius: "25px",
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
  padding: "10px ",
  boxShadow: "var(--shadow)",
  background: "white",
  "&> .right , .left": {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    minHeight: "150px",
    background: "var(--back)",
    borderRadius: "15px",
    minWidth: "70px",
    margin: "10px  0",
  },
  "& span": {
    fontSize: "14px",
    fontFamily: "arial",
  },
  "&> .center": {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    height:"100%",
    maxHeight:"140px"
  },
});
export default function index() {
  return (
    <Holder>
      <div className="right">
        <Up />
        <span>Ch</span>
        <Down />
      </div>
      <div className="center">
        <Refresh />
        <Mute />
      </div>
      <div className="left">
        <Plus />
        <span>Vol</span>
        <Minus />
      </div>
    </Holder>
  );
}
